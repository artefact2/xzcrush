default:
	@echo "See README.md for usage"
	@exit 1

%.stripped: %
	cp $< $@ || (rm -f $@; exit 1)
	strip -s $@ || (rm $@; exit 1)
	sstrip -z $@ || (rm $@; exit 1)

%.crushed: %.stripped
	mkdir $*.tmp
	./generate-xz-opts | parallel -d '\n' ./compress $< $*.tmp | pv -B 1 -ls `./generate-xz-opts | wc -l` >/dev/null
	./package-smallest $*.tmp $@ || (rm -Rf $*.tmp $@; exit 1)
	rm -Rf $*.tmp

dist-clean: clean
	rm -Rf *.crushed *.stripped *.tmp

.PHONY: default dist-clean
