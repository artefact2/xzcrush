xzcrush
=======

A simple, brute-force approach to compressing an executable file to
the smallest possible size with `xz`. Released under the WTFPLv2.

What's the catch?
=================

* It requires `xzcat` on the target machine (part of `xz-utils`,
  installed by default in many GNU/Linux distributions, including
  [Ubuntu](http://releases.ubuntu.com/17.04/ubuntu-17.04-desktop-amd64.manifest)
  and
  [Arch](http://releases.ubuntu.com/17.04/ubuntu-17.04-desktop-amd64.manifest)).

* It requires `/tmp` to be writeable and mounted without `noexec`.

Usage
=====

~~~
./xzcrush <exe-file-in> [exe-file-out]
~~~
